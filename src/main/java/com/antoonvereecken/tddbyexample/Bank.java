package com.antoonvereecken.tddbyexample;

import java.util.HashMap;

public class Bank {

    private final HashMap<Pair, Integer> rateByCurrencyPairMap = new HashMap<>();

    Money convertMoneyToCurrency(Expression source, CurrencyEnum toCurrency) {
        return source.convertMoneyIntoDifferentCurrency(this, toCurrency);
    }
    public int getRate(CurrencyEnum from, CurrencyEnum to) {
        if (from.equals(to) && to.equals(from)) return 1;
        return rateByCurrencyPairMap.get(new Pair(from, to));
    }
    public void addRate(CurrencyEnum from, CurrencyEnum to, int rate) {
        rateByCurrencyPairMap.put(new Pair(from, to), rate);
    }


}