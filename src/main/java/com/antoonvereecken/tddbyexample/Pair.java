package com.antoonvereecken.tddbyexample;

import java.util.Objects;

class Pair {
    private final CurrencyEnum from;
    private final CurrencyEnum to;

    Pair(CurrencyEnum from, CurrencyEnum to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair pair = (Pair) o;
        return from == pair.from &&
                to == pair.to;
    }
    @Override
    public int hashCode() {
        return Objects.hash(from, to);
    }
}
