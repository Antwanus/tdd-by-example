package com.antoonvereecken.tddbyexample;

public class Sum implements Expression {
    final Expression augend; // augendum - to be augmented
    final Expression addend; // addendum - to be added

    public Sum(Expression augend, Expression addend) {
        this.addend = addend;
        this.augend = augend;
    }

    @Override
    public Money convertMoneyIntoDifferentCurrency(Bank bank, CurrencyEnum toCurrency) {
        int amount =
                augend.convertMoneyIntoDifferentCurrency(bank, toCurrency).amount
                + addend.convertMoneyIntoDifferentCurrency(bank, toCurrency).amount;
        return new Money(amount, toCurrency);
    }
    @Override
    public Expression plus(Expression addend) {
        return new Sum(this, addend);
    }
    @Override
    public Expression times(int multiplier) {
        return new Sum(augend.times(multiplier), addend.times(multiplier));
    }


}