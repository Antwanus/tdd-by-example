package com.antoonvereecken.tddbyexample;

import java.util.Objects;

public class Money implements Expression {

    protected final int amount;
    protected final CurrencyEnum currencyEnum;

    public Money(int amount, CurrencyEnum currencyEnum) {
        this.currencyEnum = currencyEnum;
        this.amount = amount;
    }

    @Override
    public Money convertMoneyIntoDifferentCurrency(Bank bank, CurrencyEnum to) {
        return new Money(amount / bank.getRate(this.currencyEnum, to), to);
    }
    @Override
    public Expression plus(Expression addend) {
        return new Sum(this, addend);
    }
    public Expression plus(Money addend) {
        return new Sum(this, addend);
    }
    @Override
    public Expression times(int multiplier) {
        return new Money(amount * multiplier, this.currencyEnum);
    }
    static Money dollar(int amount){
        return new Money(amount, CurrencyEnum.USD);
    }
    static Money euro(int amount) {
        return new Money(amount, CurrencyEnum.EUR);
    }
    protected String currency() {
        return currencyEnum.name();
    }

    @Override
    public String toString() {
        return "Money{" +
                "amount=" + amount +
                ", currency='" + currencyEnum + '\'' +
                '}';
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Money money = (Money) o;
        return amount == money.amount && currencyEnum == money.currencyEnum;
    }
    @Override
    public int hashCode() {
        return Objects.hash(amount);
    }
}
