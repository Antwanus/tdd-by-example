package com.antoonvereecken.tddbyexample;

public interface Expression {
    Money convertMoneyIntoDifferentCurrency(Bank bank, CurrencyEnum toCurrency);

    Expression plus(Expression addend);

    Expression times(int multiplier);
}
