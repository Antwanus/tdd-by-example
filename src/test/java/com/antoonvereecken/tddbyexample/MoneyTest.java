package com.antoonvereecken.tddbyexample;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class MoneyTest {

   int amount;
   Bank bank;
   Expression tenBucks;
   Expression fiveEuro;

   @BeforeEach
   void setAmount() {
      amount = 5;
   }
   @BeforeEach
   void createMoney() {
      tenBucks = Money.dollar(10);
      fiveEuro = Money.euro(5);
   }
   @BeforeEach
   void createBankAndSetRate() {
      bank = new Bank();
      bank.addRate(CurrencyEnum.USD, CurrencyEnum.EUR, 2); // $2 for €1
   }

   @Test
   void testSumPlusMoney() {
      Expression sum = new Sum(fiveEuro, tenBucks).plus(tenBucks);
      Money result = bank.convertMoneyToCurrency(sum, CurrencyEnum.EUR);
      assertEquals(Money.euro(15), result);
   }
   @Test
   void testSumTimes() {
      Expression sum = new Sum(fiveEuro, tenBucks).times(2);
      Money result = bank.convertMoneyToCurrency(sum, CurrencyEnum.EUR);
      assertEquals(Money.euro(20), result);
   }
   @Test
   void testMixedCurrencyAddition() {
      Money result = bank.convertMoneyToCurrency(tenBucks.plus(fiveEuro), CurrencyEnum.EUR);
      assertEquals(Money.euro(10), result);
   }
   @Test
   void testConvertMoneyToCurrencyWithDifferentCurrency() {
      Money result = bank.convertMoneyToCurrency(Money.dollar(2), CurrencyEnum.EUR);
      assertEquals(Money.euro(1), result);
   }
   @Test
   void testSameIdentityEqualsSameRate() {
      assertEquals(1, new Bank().getRate(CurrencyEnum.USD, CurrencyEnum.USD));
      assertEquals(1, new Bank().getRate(CurrencyEnum.EUR, CurrencyEnum.EUR));
   }
   @Test
   void testPlusReturnsSum() {
      Money five = Money.dollar(amount);
      Expression result = five.plus(five);
      Sum sum = (Sum) result;
      assertEquals(five, sum.augend);
      assertEquals(five, sum.addend);
   }
   @Test
   void testReduceSum() {
      Expression sum = new Sum(Money.dollar(3), Money.dollar(4));
      Money result = bank.convertMoneyToCurrency(sum, CurrencyEnum.USD);
      assertEquals(Money.dollar(7), result);
   }
   @Test
   void testReduceMoney() {
      Bank bank = new Bank();
      Money result = bank.convertMoneyToCurrency(Money.dollar(1), CurrencyEnum.USD);
      assertEquals(Money.dollar(1), result);
   }
   @Test
   void testSimpleAddition() {
      Expression sum = fiveEuro.plus(fiveEuro);
      Money convertedMoneySumToCurrency = bank.convertMoneyToCurrency(sum, CurrencyEnum.EUR);
      assertEquals(Money.euro(amount + amount), convertedMoneySumToCurrency);
   }
   @Test
   void testCurrencyMethodInMoneyReturnsString() {
      assertEquals("USD", Money.dollar(1).currency());
      assertEquals("EUR", Money.euro(1).currency());
   }
   @Test
   void testNonEqualityDollarsAndEuros() {
      assertNotEquals(
              new Money(amount, CurrencyEnum.USD),
              new Money(amount, CurrencyEnum.EUR)
      );
   }
   @Test
   void testMultiplyMoney() {
      assertEquals(Money.dollar(20), tenBucks.times(2));
      assertEquals(Money.dollar(30), tenBucks.times(3));

      assertEquals(Money.euro(10), fiveEuro.times(2));
      assertEquals(Money.euro(15), fiveEuro.times(3));
   }
   @Test
   void testEqualityMoney() {
      assertEquals(Money.dollar(amount), Money.dollar(amount));
      assertNotEquals(Money.dollar(amount), Money.dollar(++amount));

      assertEquals(Money.euro(amount), Money.euro(amount));
      assertNotEquals(Money.euro(amount), Money.euro(++amount));
   }


}